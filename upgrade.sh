#!/bin/bash

DIRECTORY="/usr/local/bin/PCFastManage"
systemctl stop PCFastManage.timer
systemctl stop PCFastManage.service

BRANCH=${1:-master}

echo "Branch set to $BRANCH"

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade PCFastRelease the latest version"
	git -C $DIRECTORY fetch --all
	git -C $DIRECTORY reset --hard origin/$BRANCH
	git -C $DIRECTORY submodule foreach git reset --hard
	git -C $DIRECTORY submodule update --recursive

else
	echo "Install PCFastRelease"
	mkdir $DIRECTORY
	chmod 710 $DIRECTORY
	git -C $DIRECTORY clone --branch $BRANCH https://gitlab.com/JonW/PCFastManage.git .
fi

cp $DIRECTORY/PCFastManage.service /lib/systemd/system/PCFastManage.service
cp $DIRECTORY/PCFastManage.timer /lib/systemd/system/PCFastManage.timer

systemctl daemon-reload
systemctl enable PCFastManage.timer
systemctl start PCFastManage.timer