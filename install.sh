#!/bin/bash

DIRECTORY="/usr/local/bin/PCFastManage"
systemctl stop PCFastManage.service
systemctl stop PCFastManage.timer

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade PCFastManage the latest version"
	git -C $DIRECTORY pull origin master

else
	echo "Install PCFastManage"
	mkdir $DIRECTORY
	chmod 710 $DIRECTORY
	git -C $DIRECTORY clone https://gitlab.com/JonW/PCFastManage.git .
	
	cp $DIRECTORY/YonTech.xyz.CA.crt /usr/local/share/ca-certificates/YonTech.xyz.CA.crt
	update-ca-certificates
fi

cp $DIRECTORY/PCFastManage.service /lib/systemd/system/PCFastManage.service
cp $DIRECTORY/PCFastManage.timer /lib/systemd/system/PCFastManage.timer

systemctl daemon-reload
systemctl enable PCFastManage.timer