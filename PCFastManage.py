import urllib.parse
import http.client
import os.path
import shutil
import json
import base64
import ssl
import time
import subprocess
import netifaces

class Manage:
    "The Main Class"
    #Static Configuration 
    wpa_supplicant_dir = "/etc/wpa_supplicant/"
    #wpa_supplicant_dir = ""
    wpa_supplicant_cfg = wpa_supplicant_dir+"wpa_supplicant.conf"
    wpa_supplicant_cfg_good = wpa_supplicant_dir+"wpa_supplicant.conf.good"
    wpa_supplicant_cfg_backup = wpa_supplicant_dir+"wpa_supplicant.conf.bakup"
    
    wpa_supplicant_tls_cer=wpa_supplicant_dir+"cert.pfx"
    wpa_supplicant_tls_cer_good = wpa_supplicant_dir+"cert.pfx.good"

    def __init__(self):
        # Load Settings:
        self.Settings = False
        self.Load_Settings()
        if(self.Check_Network()):
            print("Network Check Passed.")
        else:
            print("Network Check Failure.")
            exit(40)
        if(self.Settings):
            print("init Sucsess")
        else:
            self.Get_Serial()
            if(self.Settings):
                print("init Sucsess")
            else:
                print("CRITICAL- Manage init Failure")
                exit(11)
        

    def Load_Settings(self):
        #Load the primary config
        try:
            with open('conf.json') as f:
                self.Settings = json.loads(f.read())
        except:
            self.Settings = False

        #if we failed to load the primary config because it
        #is corrupt or something we can fall back to the backup config 
        if(self.Settings == False):
            print("Primary config loading failed. Loading backup.")
            try:
                with open('conf.json.backup') as f:
                    self.Settings = json.loads(f.read())
            except:
                self.Settings = False

        if(self.Settings):
            if (("SerialNumber" in self.Settings) and ("Certfile" in self.Settings) and ("CertKey" in self.Settings)):    
                if (("ConfigVersion" in self.Settings) and ("ConfigVersionSucsess" in self.Settings)):
                    print("ConfigVersion and Sucsess found.")
                else:
                    print("No ConfigVersion or ConfigVersionSucsess settings")
                    self.Settings.update({'ConfigVersion':None,'ConfigVersionSucsess':None})
                print("Settings Loaded Sucsess")
                return True
            else:
                print("Failure. Settings missing data.")
                return False 
        else:
            print("Failure. Settings Loading Failed")
            return False

    def Save_Settings(self,Updates={},Backup=False):
        "Update setting file with new variables"

        if(self.Settings):
            print("update Settings")
            self.Settings.update(Updates)
        else:
            print("Replace settings")
            self.Settings = Updates

        try:    
            with open('conf.json', 'w') as f:
                f.write(json.dumps(self.Settings))
        except:
            print("Save Failed")
            return False

        if(Backup):
            print("Save Backup config")
            try:    
                with open('conf.json.backup', 'w') as f:
                    f.write(json.dumps(self.Settings))
            except:
                print("Save Backup Failed")

        self.Load_Settings()
        if(self.Settings):
            print("Save Compleate")
            return True
        else:
            print("Save Failure")
            return False

    #http://stackoverflow.com/questions/4128144/replace-string-within-file-contents
    #Adam Matan
    def inplace_change(self,filename, old_string, new_string):
        "Replace a string in a file and also create a backup file at the same time."
        with open(filename, 'rt') as f:
            string = f.read()

        # save the file to backup file
        with open(filename+".backup", 'wt') as f:
            f.write(string)

        # Safely write the changed content, if found in the file
        with open(filename, 'wt') as f:
            print ("Changing "+old_string+" to "+new_string+" in "+filename)
            string = string.replace(old_string, new_string)
            f.write(string)

    def Get_Current_SSID(self):
        "Get the current ssid that is connected to"
        subp = subprocess.Popen("/sbin/iwgetid -r", stdout=subprocess.PIPE, shell=True)
        (SSID, err) = subp.communicate()
        return (SSID.decode("utf-8")).rstrip()

    def Get_Interface_Arrd(self):
        "Get the current ethernet addresses"
        Interface_List = {}
        for val in netifaces.interfaces():
            try:
                addr = netifaces.ifaddresses(val)[2][0]['addr']
            except:
                addr = False
            Interface_List.update({val:addr})
        return Interface_List
    
    def Copy_File(self,source_path,destination_path):
        if(os.path.exists(source_path)):
            try:
                shutil.copyfile(source_path, destination_path)
            except:
                print("Copy_File: Copy Failed")
                return False
            print("Copy_File: "+str(source_path)+" copied to "+str(destination_path))
            return True
        else:
            print("Copy_File: "+str(source_path)+" File Does not exist")
            return False

    def Restart_Networking(self):
        "Restarts the network interfaces"
        print("Networking Reload")
        subprocess.call("/usr/sbin/service networking reload", shell=True)
        print("Wait for Network")
        time.sleep(10)
    
    def Restart_Device(self):
        "Restarts the Device"
        print("Networking Reload")
        subprocess.call("/sbin/reboot", shell=True)
        
    def Invoke_API_request(self,RequestType):
        "invoke the api request."
        #Every time a request is done we send status info
        #Generate Status Info
        CurrentSSID = json.dumps(self.Get_Current_SSID())
        CurrentIP = json.dumps(self.Get_Interface_Arrd())

        #These are sent every Request
        values = {'RequestType' : RequestType,'CurrentSSID' : CurrentSSID,'CurrentIP' : CurrentIP}

        #These are sent on a Send-Config-Status Report
        if (RequestType == "Set-Status" ):
            print("Adding data for config status report: ConfigVersion="+str(self.Settings["ConfigVersion"])+" ConfigVersionSucsess="+str(self.Settings["ConfigVersionSucsess"]))
            values.update({'ConfigVersion':self.Settings["ConfigVersion"],'ConfigVersionSucsess':self.Settings["ConfigVersionSucsess"]})
            
        values = urllib.parse.urlencode(values)
        headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}

        if(RequestType == "Get-Serial"):
            conn = http.client.HTTPSConnection('enroll.yontec.xyz')
        else:
            if(self.Settings["Certfile"] and self.Settings["CertKey"]):
                sslcontext = ssl.create_default_context()
                sslcontext.load_cert_chain(self.Settings["Certfile"], keyfile=self.Settings["CertKey"])
                conn = http.client.HTTPSConnection('pnp.yontec.xyz',context=sslcontext)
            else:
                print("FATAL- No certificates")
            
        try:
            conn.request("POST", "/v1/api.php", values, headers)
        except Exception as e:
            print("API Conection failure restart wifi."+str(e))
            return False
        response = conn.getresponse()
        html = response.read()
        #print(html)
        if (response.status == 200):
            print("API request sucsess status",response.status)
            return json.loads(html.decode("utf-8"))
        else:
            print("API request failed status",response.status)
            return False
    
    def Check_Network(self):
        Interfaces = self.Get_Interface_Arrd()
        if("eth0" in Interfaces):
            self.HasWifi = False
            if(Interfaces["eth0"]):
                print("eth0 has ip address "+str(Interfaces))
                return True
            else:
                print("eth0 has no ip address."+str(Interfaces))
                self.Restart_Networking()
                return False
        elif("wlan0" in Interfaces):
            self.HasWifi = True
            if(Interfaces["wlan0"]):
                print("wlan0 has ip address "+str(Interfaces))
                return True
            else:
                print("wlan0 has no ip address."+str(Interfaces))
                self.Restart_Networking()
                return False

    def Get_Status(self):
        result = self.Invoke_API_request('Get-Status')
        if (result[0]["Sucsess"]):
            print(result)
            if ((result[0]["Config_TimeStamp"] != self.Settings["ConfigVersion"]) or (self.Settings["ConfigVersionSucsess"] == False)):
                print("config does not match the applied config. Update the local config.")
                if (result[0]["Config_Ready"]):
                    print("The Config is marked as ready ok to apply.")    
                    self.Get_Config()
                else:
                    print("The config is marked by the api as not ready.")
            else:
                print("The applied config has not changed")
        else:
            print ("Request Failure, Error Number ",result[0]["Error"])
            exit(2)

    def Get_Serial(self):
        result = self.Invoke_API_request('Get-Serial')
        if (result[0]["Sucsess"] and result[0]["PubName"] and result[0]["PubData"] and result[0]["KeyName"] and result[0]["KeyData"] and result[0]["Serial"]):
            print ("Request Sucess, Device Serial number",result[0]["Serial"])
            print ("Request Sucess, Certificate Serial number",result[0]["CertSerial"])
            print("Writing",result[0]["PubName"],"to disk")
            try:
                with open(result[0]["PubName"], "wb") as Pub:
                    Pub.write(base64.decodebytes(bytes(result[0]["PubData"], 'utf-8')))
            except:
                print("FATAL- Pub Key write Failed")
                exit(21)

            try:
                with open(result[0]["KeyName"], "wb") as Key:
                    Key.write(base64.decodebytes(bytes(result[0]["KeyData"], 'utf-8')))
            except:
                print("FATAL- Pri Key write Failed")
                exit(22)

            if(self.Save_Settings({'SerialNumber':result[0]["Serial"],'Certfile':result[0]["PubName"],'CertKey':result[0]["KeyName"]},True)):
                #/etc/hosts
                self.inplace_change("/etc/hosts","raspberrypi",str(self.Settings["SerialNumber"]))
                self.inplace_change("/etc/hosts","localhost nanopineo",(str(self.Settings["SerialNumber"])+" localhost"))
                #/etc/hostname
                self.inplace_change("/etc/hostname","raspberrypi",str(self.Settings["SerialNumber"]))
                self.inplace_change("/etc/hostname","nanopineo",str(self.Settings["SerialNumber"]))
                #now set the hostname of the Pi
                subprocess.call("/etc/init.d/hostname.sh", shell=True)
                #Now Restart the device
                #self.Restart_Device()
            else:
                print("FATAL- Save Failure")
                exit(23)
        else:
            print ("FATAL- Request Failure or data missing, Error Number ",result[0]["Error"])
            exit(24)

    def Get_Config(self):
        result = self.Invoke_API_request('Get-Config')
        if (result[0]["Sucsess"] and result[0]["Config_Ready"]):
            if(self.HasWifi):
                print("Wifi Configuration")
                #print(result)
                print("The WPA suplicant file is",self.wpa_supplicant_cfg_backup)
                #Read in the default Wifi configuration
                with open(self.wpa_supplicant_cfg_backup, "r",encoding='utf-8') as wpa_suplicant_bakcup:
                    wpa_suplicant = wpa_suplicant_bakcup.read()
                #print(wpa_suplicant)
                
                #Create the new wifi profile network section
                wpa_suplicant = wpa_suplicant + "network={\n"
                
                #Set the SSID
                print("Writing ssid to profile")
                wpa_suplicant = wpa_suplicant + ' ssid="'+result[0]["Config_WiFi_ssid"]+'"'+"\n"

                #set the scan_ssid option
                if(result[0]["Config_WiFi_scan_ssid"] == "0" or result[0]["Config_WiFi_scan_ssid"] == "1"):
                    print("Writing scan_ssid to profile")
                    wpa_suplicant = wpa_suplicant + " scan_ssid="+result[0]["Config_WiFi_scan_ssid"]+"\n"
                else:
                    print("an unknown value for scan_ssid was set (default to 0)")
                    wpa_suplicant = wpa_suplicant + " scan_ssid=0\n"

                if (result[0]["Config_WiFi_key_mgmt"]):
                    print("Writing key management protocols")
                    wpa_suplicant = wpa_suplicant + ' key_mgmt='
                    for val in result[0]["Config_WiFi_key_mgmt"]:
                        wpa_suplicant = wpa_suplicant + val +' '
                    wpa_suplicant = wpa_suplicant + "\n"

                if (result[0]["Config_WiFi_pairwise"]):
                    print("Writing pairwise (unicast) ciphers for WPA to profile")
                    wpa_suplicant = wpa_suplicant + ' pairwise='
                    for val in result[0]["Config_WiFi_pairwise"]:
                        wpa_suplicant = wpa_suplicant + val +' '
                    wpa_suplicant = wpa_suplicant + "\n"
                
                if (result[0]["Config_WiFi_group"]):
                    print("Writing group (multicast) ciphers to profile")
                    wpa_suplicant = wpa_suplicant + ' group='
                    for val in result[0]["Config_WiFi_group"]:
                        wpa_suplicant = wpa_suplicant + val +' '
                    wpa_suplicant = wpa_suplicant + "\n"
                        
                #set the PSK if it is set in the config
                if(result[0]["Config_WiFi_psk"]):   
                    print("Writing PSKto profile")
                    wpa_suplicant = wpa_suplicant + ' psk="'+result[0]["Config_WiFi_psk"]+'"'+"\n"
                    
                #wpa_suplicant = wpa_suplicant + ' eap='+result[0]["Config_WiFi_eap"]+"\n"
                if (result[0]["Config_WiFi_eap"]):
                    wpa_suplicant = wpa_suplicant + ' eap='
                    for val in result[0]["Config_WiFi_eap"]:
                        wpa_suplicant = wpa_suplicant + val +' '
                    wpa_suplicant = wpa_suplicant + "\n"
                        
                #WiFi_identity
                if(result[0]["Config_WiFi_identity"]):
                    print("Writing EAP Identity (username) to profile")
                    wpa_suplicant = wpa_suplicant + ' identity="'+result[0]["Config_WiFi_identity"]+'"'+"\n"

                #anonymous_identity
                if(result[0]["Config_WiFi_anonymous_identity"]):
                    print("Writing EAP Anonymous Identity to profile")
                    wpa_suplicant = wpa_suplicant + ' anonymous_identity="'+result[0]["Config_WiFi_anonymous_identity"]+'"'+"\n"

                #EAP password (put this with the check for eap type)
                if(result[0]["Config_WiFi_password"]):
                    print("Writing EAP Password to profile")
                    wpa_suplicant = wpa_suplicant + ' password="'+result[0]["Config_WiFi_password"]+'"'+"\n"

                #create the certificate required for wifi
                if (result[0]["Config_WiFi_cert"] and result[0]["Config_WiFi_cert_pass"]):
                    print("Writing certificate to disk")
                    with open(self.wpa_supplicant_tls_cer, "wb") as cert_file:
                        cert_file.write(base64.decodebytes(bytes(result[0]["Config_WiFi_cert"], 'utf-8')))
                    print("Writing certificate info to profile")
                    #wpa_suplicant = wpa_suplicant + ' ca_cert="'+wpa_supplicant_tls_ca_cer+'"'+"\n"
                    #wpa_suplicant = wpa_suplicant + ' client_cert="'+self.wpa_supplicant_tls_cer+'"'+"\n"
                    wpa_suplicant = wpa_suplicant + ' private_key="'+self.wpa_supplicant_tls_cer+'"'+"\n"
                    wpa_suplicant = wpa_suplicant + ' private_key_passwd="'+result[0]["Config_WiFi_cert_pass"]+'"'+"\n"
                else:
                    print("No Certificate in payload")

                #Set the profile priority
                if(result[0]["Config_WiFi_priority"]):
                    print("Writing Priority to profile")
                    wpa_suplicant = wpa_suplicant + ' priority='+result[0]["Config_WiFi_priority"]+"\n"
                
                wpa_suplicant = wpa_suplicant + "}"
                #print(wpa_suplicant)
        
                with open(self.wpa_supplicant_cfg, "w",encoding='utf-8') as wpa_suplicant_new:
                    print("Writing conf to disk")
                    wpa_suplicant_new.write(wpa_suplicant)

                with open(self.wpa_supplicant_cfg+".debug", "w",encoding='utf-8') as wpa_suplicant_new:
                    print("Writing debug conf to disk")
                    wpa_suplicant_new.write(wpa_suplicant)

                #print the new config.
                print(wpa_suplicant)
                
                #Now we reload the wifi configuration and make sure we connect to the expected network
                print("Current SSID =",self.Get_Current_SSID())
                print("Expected SSID =",result[0]["Config_WiFi_ssid"])

                self.Restart_Networking()

                if (self.Get_Current_SSID() == result[0]["Config_WiFi_ssid"]):
                    print("Sucsessfully joined",result[0]["Config_WiFi_ssid"])
                    #save the version of the config that has been applied
                    self.Set_Status(result[0]["Config_TimeStamp"],True)
                    
                    #Backup the good config
                    self.Copy_File(self.wpa_supplicant_cfg,self.wpa_supplicant_cfg_good)
                    #Backup the good Certificate
                    self.Copy_File(self.wpa_supplicant_tls_cer,self.wpa_supplicant_tls_cer_good)

                else:
                    #we should send the failure after the last good configuration is re applied
                    print("Failure")
                    print("Current SSID =",self.Get_Current_SSID())
                    print("Expected SSID =",result[0]["Config_WiFi_ssid"])
                    
                    if (self.Copy_File(self.wpa_supplicant_cfg_good,self.wpa_supplicant_cfg)):
                        print("Restored the last known good config")
                        if(self.Copy_File(self.wpa_supplicant_tls_cer_good,self.wpa_supplicant_tls_cer)):
                            print("Restored the last known good certificate")
                        else:
                            print("Failed to restore good certificate.")
                    else:
                        if(self.Copy_File(self.wpa_supplicant_cfg_backup,self.wpa_supplicant_cfg)):
                            print("Restored the default configuration")
                        else:
                            print("FATAL- Oh no the default configuration failed to copy")
                            exit(31)

                    self.Restart_Networking()
                    
                    #Tell the server that the config failed
                    self.Set_Status(result[0]["Config_TimeStamp"],False)

                    #finally we should try a reboot.
                    self.Restart_Device()
            else:
                #Lets set the wifi config as applied as we run on ethernet but if there is other settings like proxy settings we could set them in this area.
                print("Skipping Wifi Configuration")
                self.Set_Status(result[0]["Config_TimeStamp"],True)
        else:
            print ("Request Failure or data missing, Error Number ",result[0]["Error"])
            exit(2)

    def Set_Status(self,ConfigVersion,ConfigVersionSucsess):
        #Save the status to config
        self.Save_Settings({'ConfigVersion':ConfigVersion,'ConfigVersionSucsess':ConfigVersionSucsess})
        result = self.Invoke_API_request('Set-Status')
        if (result[0]["Sucsess"]):
            print(str(result))
            return True
        else:
            print ("Request Failure, Error Number ",str(result[0]["Error"]))
            return False

manage = Manage()

manage.Get_Status()